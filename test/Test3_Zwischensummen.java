import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test3_Zwischensummen
{
    @Test
    public void testGetZwischensumme()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        kasse.scan(produkt1);
        assertEquals(250, kasse.getZwischensumme());

        Produkt produkt2 = new Produkt("Banane", 130);
        kasse.scan(produkt2);
        assertEquals(380, kasse.getZwischensumme());

        Produkt produkt3 = new Produkt("Ananas", 570);
        kasse.scan(produkt3);
        assertEquals(950, kasse.getZwischensumme());
    }
}
