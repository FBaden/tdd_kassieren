import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBonus2_Tagesbilanz
{
    @Test
    public void testAnzahlKunden()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.bezahlen();

        Produkt produkt3 = new Produkt("Kirsche", 150);
        kasse.scan(produkt3);
        kasse.bezahlen();

        Produkt produkt4 = new Produkt("Avocado", 500);
        Produkt produkt5 = new Produkt("Ananas", 600);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        kasse.bezahlen();
        assertEquals(3, kasse.getKundenzahl());
    }

    @Test
    public void testGesamtumsatz()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.bezahlen();

        Produkt produkt3 = new Produkt("Kirsche", 150);
        kasse.scan(produkt3);
        kasse.bezahlen();

        Produkt produkt4 = new Produkt("Avocado", 500);
        Produkt produkt5 = new Produkt("Ananas", 600);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        kasse.bezahlen();
        assertEquals(1800, kasse.getGesamtumsatz());
    }

    @Test
    public void testUmsatzDurchschnitt()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.bezahlen();

        Produkt produkt3 = new Produkt("Kirsche", 150);
        kasse.scan(produkt3);
        kasse.bezahlen();

        Produkt produkt4 = new Produkt("Avocado", 500);
        Produkt produkt5 = new Produkt("Ananas", 600);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        kasse.bezahlen();
        assertEquals(600, kasse.getUmsatzDurchschnitt());
    }

    @Test
    public void testGesamtRabatt()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250, false);
        Produkt produkt2 = new Produkt("Birne", 300, true);
        Produkt produkt3 = new Produkt("Kirsche", 150, false);
        Produkt produkt4 = new Produkt("Avocado", 500, true);
        Produkt produkt5 = new Produkt("Ananas", 600, true);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.scan(produkt3);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        kasse.bezahlen();
        // Kunde1: 0.90€ Rabatt durch Treueprodukte

        Produkt produkt6 = new Produkt("Banane", 200, false);
        Produkt produkt7 = new Produkt("Grünkohl", 550, true);
        kasse.scan(produkt6);
        kasse.scan(produkt7);
        kasse.gutschein();
        kasse.bezahlen();
        // Kunde2: 0.75€ Rabatt durch Gutschein

        assertEquals(165, kasse.getGesamtrabatt());
    }

    // Guter Kunde: Einkauf > 100€
    @Test
    public void testGuteKunden()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 9500);
        Produkt produkt2 = new Produkt("Birne", 3000);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.bezahlen();

        Produkt produkt3 = new Produkt("Kirsche", 6500);
        kasse.scan(produkt3);
        kasse.bezahlen();

        Produkt produkt4 = new Produkt("Avocado", 5000);
        Produkt produkt5 = new Produkt("Ananas", 6000);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        kasse.bezahlen();
        assertEquals(2, kasse.getGuterKunde());

    }

}
