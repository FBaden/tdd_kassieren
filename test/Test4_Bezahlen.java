import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test4_Bezahlen
{
    @Test
    public void testBezahlen()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        assertEquals(550, kasse.bezahlen());
        assertEquals(0, kasse.getZwischensumme());
    }
}
