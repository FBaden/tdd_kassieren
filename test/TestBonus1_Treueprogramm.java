import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBonus1_Treueprogramm
{
    // Treueprodukte >10€ --> 5% Rabatt auf gesamten Einkauf
    @Test
    public void testTreueprogramm1()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250, false);
        Produkt produkt2 = new Produkt("Birne", 300, true);
        Produkt produkt3 = new Produkt("Kirsche", 150, false);
        Produkt produkt4 = new Produkt("Avocado", 500, true);
        Produkt produkt5 = new Produkt("Ananas", 600, true);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.scan(produkt3);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        assertEquals(1710, kasse.bezahlen());
    }

    // Treueprodukte <10€ --> kein Rabatt
    @Test
    public void testTreueprogramm2()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250, false);
        Produkt produkt2 = new Produkt("Birne", 300, true);
        Produkt produkt3 = new Produkt("Kirsche", 150, false);
        Produkt produkt4 = new Produkt("Avocado", 500, true);
        Produkt produkt5 = new Produkt("Ananas", 600, false);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.scan(produkt3);
        kasse.scan(produkt4);
        kasse.scan(produkt5);
        assertEquals(1800, kasse.bezahlen());
    }

}
