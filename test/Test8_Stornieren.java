import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test8_Stornieren
{
    @Test
    public void testStornieren()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        Produkt produkt3 = new Produkt("Kirsche", 150);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.scan(produkt3);
        kasse.stornieren();
        assertEquals(550, kasse.bezahlen());
    }

}
