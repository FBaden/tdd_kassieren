import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test2_Zwischensumme
{
    @Test
    public void testGetZwischensumme()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        kasse.scan(produkt1);
        assertEquals(250, kasse.getZwischensumme());
    }

}
