import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test7_Gutschein
{
    // Gutschein vor Produkt gescannt
    @Test
    public void testGutschein1()
    {
        Kasse kasse = new Kasse();
        kasse.gutschein();
        Produkt produkt1 = new Produkt("Banane", 350);
        kasse.scan(produkt1);
        assertEquals(315, kasse.bezahlen());
    }

    // Gutschein nach Produkt gescannt
    @Test
    public void testGutschein2()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Avocado", 550);
        kasse.scan(produkt1);
        kasse.gutschein();
        assertEquals(495, kasse.bezahlen());
    }

    // Gutschein zwischen Produkten gescannt
    @Test
    public void testGutschein3()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.gutschein();
        Produkt produkt3 = new Produkt("Kirsche", 150);
        kasse.scan(produkt3);
        assertEquals(630, kasse.bezahlen());
    }
}
