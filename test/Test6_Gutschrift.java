import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test6_Gutschrift
{
    // Gutschrift nach Scan der Produkte
    @Test
    public void testGutschrift1()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        kasse.gutschrift(20);
        assertEquals(530, kasse.bezahlen());
    }

    // Gutschrift vor Scan der Produkte
    @Test
    public void testGutschrift2()
    {
        Kasse kasse = new Kasse();
        kasse.gutschrift(20);
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        assertEquals(530, kasse.bezahlen());
    }
}
