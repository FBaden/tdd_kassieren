import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test5_BezahlenWechselgeld
{
    @Test
    public void testWechselgeld()
    {
        Kasse kasse = new Kasse();
        Produkt produkt1 = new Produkt("Apfel", 250);
        Produkt produkt2 = new Produkt("Birne", 300);
        kasse.scan(produkt1);
        kasse.scan(produkt2);
        assertEquals(50, kasse.bezahlen(600));
    }
}
