import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Test1_NamePreis
{
    @Test
    public void testGetName()
    {
        Produkt produkt = new Produkt("Apfel", 250);
        assertEquals("Apfel", produkt.getName());
    }

    @Test
    public void testGetPreis()
    {
        Produkt produkt = new Produkt("Apfel", 250);
        assertEquals(250, produkt.getPreis());
    }
}
