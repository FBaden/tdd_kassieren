public class Kasse
{
    private int zwischensumme;
    private Produkt letztesProdukt;
    private int gesamtbetrag;
    private boolean gutschein = false;
    private int treueProdukteGesamt;
    private int kundenzahl = 0;
    private int gesamtumsatz = 0;
    private int gesamtrabatt = 0;
    private int guterKunde = 0;

    public void scan(Produkt produkt)
    {
        this.zwischensumme += produkt.getPreis();
        this.letztesProdukt = produkt;

        if (produkt.getTreueprodukt())
        {
            this.treueProdukteGesamt += produkt.getPreis();
        }
    }

    public int getZwischensumme()
    {
        return zwischensumme;
    }

    public int bezahlen()
    {
        if (treueProdukteGesamt >= 1000)
        {
            int rabatt = this.zwischensumme * 5 / 100;
            this.zwischensumme -= rabatt;
            this.gesamtrabatt += rabatt;
        }

        if (!gutschein)
        {
            this.gesamtbetrag = zwischensumme;
            this.zwischensumme = 0;
        } else if (gutschein)
        {
            int rabatt = this.zwischensumme * 10 / 100;
            this.gesamtbetrag = zwischensumme - rabatt;
            this.gesamtrabatt += rabatt;
            this.zwischensumme = 0;
        }

        if (gesamtbetrag > 10000)
        {
            guterKunde++;
        }

        this.kundenzahl++;
        this.gutschein = false;
        this.treueProdukteGesamt = 0;
        this.gesamtumsatz += gesamtbetrag;
        return gesamtbetrag;
    }

    public int bezahlen(int gezahlterBetrag)
    {
        return gezahlterBetrag - bezahlen();
    }

    public void gutschrift(int gutschrift)
    {
        this.zwischensumme -= gutschrift;
    }

    public void gutschein()
    {
        this.gutschein = true;
    }

    public void stornieren()
    {
        this.zwischensumme -= letztesProdukt.getPreis();
    }

    public int getKundenzahl()
    {
        return kundenzahl;
    }

    public int getGesamtumsatz()
    {
        return gesamtumsatz;
    }

    public int getUmsatzDurchschnitt()
    {
        return gesamtumsatz / kundenzahl;
    }

    public int getGesamtrabatt()
    {
        return gesamtrabatt;
    }

    public int getGuterKunde()
    {
        return guterKunde;
    }

}
