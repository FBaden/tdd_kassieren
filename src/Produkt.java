public class Produkt
{
    private String name;
    private int preis;
    private boolean treueProdukt;

    public Produkt(String name, int preis)
    {
        this.name = name;
        this.preis = preis;
    }

    public Produkt(String name, int preis, boolean treueProdukt)
    {
        this.name = name;
        this.preis = preis;
        this.treueProdukt = treueProdukt;
    }

    public String getName()
    {
        return name;
    }

    public int getPreis()
    {
        return preis;
    }

    public boolean getTreueprodukt()
    {
        return treueProdukt;
    }

}
